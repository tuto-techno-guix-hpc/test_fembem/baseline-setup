Bootstrap: debootstrap
OSVersion: bullseye
MirrorURL: http://deb.debian.org/debian/
Include: git wget gpg ca-certificates

%environment
  # Set the locale.
  export LC_ALL=C
  # Set up the environment.
  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/
  # Use sequential BLAS by default.
  export OMP_NUM_THREADS=1

%post -c /bin/bash
  # Update software sources.
  apt update
  # Install pre-built dependencies.
  apt install -y build-essential pkg-config python3 python3-dev \
  python3-pygments flex bison r-base r-base-dev r-cran-ggplot2 r-cran-svglite \
  inkscape texlive-full latexmk libhwloc15 libhwloc-dev cmake openmpi-bin \
  openmpi-common openmpi-doc libopenmpi3 libopenmpi-dev libopenblas-dev \
  liblapacke-dev
  # Create a directory to store source files in and navigate to it.
  mkdir /src
  cd /src
  # Download source files to build from.
  wget https://github.com/jemalloc/jemalloc/releases/download/4.5.0/jemalloc-4.5.0.tar.bz2
  wget https://files.inria.fr/starpu/starpu-1.3.9/starpu-1.3.9.tar.gz
  wget https://gitlab.inria.fr/solverstack/chameleon/uploads/b299d6037d7636c6be16108c89bc2aab/chameleon-1.1.0.tar.gz
  git clone https://github.com/jeromerobert/hmat-oss
  cd hmat-oss
  git checkout 6ad59b7db2be69ac3f60e4a58795b71855e1b523
  cd ..
  git clone https://gitlab.inria.fr/solverstack/test_fembem
  cd test_fembem
  git checkout 59d1983493a95ec483b4d931afcc70b687474aa5
  cd ..
  # Build and install 'jemalloc'.
  mkdir jemalloc
  tar -xvf jemalloc-4.5.0.tar.bz2 -C jemalloc --strip-components 1
  cd jemalloc
  ./configure
  make -j$(nproc) install
  cd ..
  # Build and install 'StarPU'.
  mkdir starpu
  tar -xvzf starpu-1.3.9.tar.gz -C starpu --strip-components 1
  cd starpu
  ./configure --enable-maxcpus=128 --disable-build-examples \
              --disable-build-doc
  make -j$(nproc) install
  cd ..
  # Build and install 'Chameleon'.
  mkdir chameleon
  tar -xvf chameleon-1.1.0.tar.gz -C chameleon --strip-components 1
  cd chameleon
  mkdir build
  cd build
  cmake -DCHAMELEON_USE_MPI=ON -DBLA_VENDOR=OpenBLAS \
        -DBUILD_SHARED_LIBS=ON ..
  make -j$(nproc) install
  cd ../..
  # Build and install 'HMAT-OSS'.
  cd hmat-oss
  mkdir build
  cd build
  cmake -DHMAT_JEMALLOC=ON -DHMAT_EXPORT_BUILD_DATE=ON ..
  make -j$(nproc) install
  cd ../..
  # Build and install 'test_FEMBEM'.
  cd test_fembem
  mkdir build
  cd build
  cmake ..
  make -j$(nproc) install

%test
  # Set up the environment.
  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/
  # Use sequential BLAS by default.
  export OMP_NUM_THREADS=1
  # Run a 'test_FEMBEM' test case.
  test_FEMBEM -nbpts 2000 -z --hmat --fem -solvehmat

%labels
  Author "Marek Felšöci <marek.felsoci@inria.fr>"

%help
  This container provides the open-source version of the 'test_FEMBEM' solver
  test suite linked to the HMAT-OSS and Chameleon solvers and using the
  open-source math libraries OpenBLAS and LAPACKE.
