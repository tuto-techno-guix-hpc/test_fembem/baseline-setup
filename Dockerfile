FROM debian:bullseye
LABEL author="Marek Felšöci <marek.felsoci@inria.fr>"

# Use Bash shell instead of the default 'sh' shell.
SHELL [ "/bin/bash", "-c" ]
ENV SHELL=/bin/bash

# Set up the environment.
ENV DEBIAN_FRONTEND="noninteractive" TZ="Europe/Bratislava"
ENV LANG="sk_SK.UTF-8" LANGUAGE="sk"
ENV SOURCES=/src
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/
# Use sequential BLAS by default.
ENV OMP_NUM_THREADS=1

# Update software sources.
RUN apt update
# Install locales.
RUN apt install -y locales-all
# Install pre-built dependencies.
RUN apt install -y build-essential pkg-config python3 python3-dev \
    python3-pygments flex bison r-base r-base-dev r-cran-ggplot2 r-cran-svglite \
    inkscape texlive-full latexmk libhwloc15 libhwloc-dev cmake openmpi-bin \
    openmpi-common openmpi-doc libopenmpi3 libopenmpi-dev libopenblas-dev \
    liblapacke-dev git wget gpg ca-certificates
# Create a directory to store source files in and set it as current working
# directory.
WORKDIR ${SOURCES}
# Download source files to build from.
RUN wget https://github.com/jemalloc/jemalloc/releases/download/4.5.0/jemalloc-4.5.0.tar.bz2
RUN wget https://files.inria.fr/starpu/starpu-1.3.9/starpu-1.3.9.tar.gz
RUN wget https://gitlab.inria.fr/solverstack/chameleon/uploads/b299d6037d7636c6be16108c89bc2aab/chameleon-1.1.0.tar.gz
RUN git clone https://github.com/jeromerobert/hmat-oss
WORKDIR ${SOURCES}/hmat-oss
RUN git checkout 6ad59b7db2be69ac3f60e4a58795b71855e1b523
WORKDIR ${SOURCES}
RUN git clone https://gitlab.inria.fr/solverstack/test_fembem
WORKDIR ${SOURCES}/test_fembem
RUN git checkout 59d1983493a95ec483b4d931afcc70b687474aa5
WORKDIR ${SOURCES}
# Build and install 'jemalloc'.
RUN mkdir jemalloc
RUN tar -xvf jemalloc-4.5.0.tar.bz2 -C jemalloc --strip-components 1
WORKDIR ${SOURCES}/jemalloc
RUN ./configure
RUN make -j$(nproc) install
WORKDIR ${SOURCES}
# Build and install 'StarPU'.
RUN mkdir starpu
RUN tar -xvzf starpu-1.3.9.tar.gz -C starpu --strip-components 1
WORKDIR ${SOURCES}/starpu
RUN ./configure --enable-maxcpus=128 --disable-build-examples \
                --disable-build-doc
RUN make -j$(nproc) install
WORKDIR ${SOURCES}
# Build and install 'Chameleon'.
RUN mkdir chameleon
RUN tar -xvf chameleon-1.1.0.tar.gz -C chameleon --strip-components 1
WORKDIR ${SOURCES}/chameleon
WORKDIR ${SOURCES}/chameleon/build
RUN cmake -DCHAMELEON_USE_MPI=ON -DBLA_VENDOR=OpenBLAS \
          -DBUILD_SHARED_LIBS=ON ..
RUN make -j$(nproc) install
WORKDIR ${SOURCES}
# Build and install 'HMAT-OSS'.
WORKDIR ${SOURCES}/hmat-oss
WORKDIR ${SOURCES}/hmat-oss/build
RUN cmake -DHMAT_JEMALLOC=ON -DHMAT_EXPORT_BUILD_DATE=ON ..
RUN make -j$(nproc) install
WORKDIR ${SOURCES}
# Build and install 'test_FEMBEM'.
WORKDIR ${SOURCES}/test_fembem
WORKDIR ${SOURCES}/test_fembem/build
RUN cmake ..
RUN make -j$(nproc) install